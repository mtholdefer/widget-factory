package main.java.model;

public class Item {
    public int id;
    public float price;
    public String widget;
    public String category;
    public int quantity;
}
