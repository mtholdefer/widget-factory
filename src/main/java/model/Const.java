package main.java.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Const {
    //Constant variables used to access the database
    public static final String DB_NAME = "widgetdb";
    public static final String INVENTORY = "inventory";
    public static final String ID = "id";
    public static final String PRICE = "price";
    public static final String WIDGET = "widget";
    public static final String WIDGET_ID = "widgetid";
    public static final String ORDERS = "orders";
    public static final String QUANTITY = "quantity";
    public static final String CATEGORY = "category";
    public static final String ATTRIBUTE = "attribute";
    public static final Set<String> REQUIRED_FEILDS = new HashSet<>(Arrays.asList(WIDGET,QUANTITY,CATEGORY,PRICE,ID));
}
