package rest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.swagger.annotations.*;
import main.java.model.Const;
import main.java.model.Item;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@RestController
@RequestMapping("/api")
public class DataController {

    private static Gson gson = new Gson();


    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully received request and sent proper response"),
            @ApiResponse(code = 400, message = "The request you sent was formatted incorrectly, or did not contain proper parameters"),
            @ApiResponse(code = 500, message = "The query for all data failed")
    })

    @ApiOperation(value = "Returns all widgets that are currently in stock", response = Item.class)
    @RequestMapping(value = "/data/available", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getAllAvailableData() {
        ArrayList<Object> json = null;
        try {
            String query = createBaseQuery() + " where " + Const.DB_NAME + "." + Const.INVENTORY + "." + Const.QUANTITY +" > 0" ;
            PreparedStatement availableData = rest.WidgetApplication.conn.prepareStatement(query);
            ResultSet resultSet = availableData.executeQuery();
            json = convertResultSetToJson(resultSet);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(gson.toJson(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Map<String, Object> result = new HashMap<>();

        result.put("result", json);

        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully received request and sent proper response"),
            @ApiResponse(code = 400, message = "The request you sent was formatted incorrectly, or did not contain proper parameters"),
            @ApiResponse(code = 500, message = "The query for all data failed")
    })

    @ApiOperation(value = "Returns all widgets, regardless of what is in stock", response = Item.class)
    @RequestMapping(value = "/data/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getAllData() {
        ArrayList<Object> json = null;
        try {
            String query = createBaseQuery();
            PreparedStatement allData = rest.WidgetApplication.conn.prepareStatement(query);
            ResultSet resultSet = allData.executeQuery();
            json = convertResultSetToJson(resultSet);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(gson.toJson(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Map<String, Object> result = new HashMap<>();

        result.put("result", json);

        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }

    @ApiOperation(value = "Sets the quantity of the specified item to the inputed amount")
    @RequestMapping(value = "/data/set/quantity", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> setQuantity(@RequestParam("itemId") int itemId, @RequestParam("quantity") int quantity) {
        try {
            String query = "UPDATE " +  Const.DB_NAME + "." + Const.INVENTORY +  " SET " +
                    Const.DB_NAME + "." + Const.INVENTORY + "." + Const.QUANTITY + " = ?" + " WHERE " + Const.ID + "= ?";

            // Create prepared statement with parameterized query
            PreparedStatement quantityStatement = rest.WidgetApplication.conn.prepareStatement(query);

            // Fill in parameters with category values, as to protect against SQL injection.
            quantityStatement.setInt(1, quantity);
            quantityStatement.setInt(2, itemId);
            quantityStatement.execute();

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(gson.toJson(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Map<String, Object> result = new HashMap<>();

        result.put("result", "Success, new quantity: " + quantity);

        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }

    @ApiOperation(value = "Takes the name of an attribute and the value to add.")
    @RequestMapping(value = "/data/value/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> addValue(@RequestParam("table") String table, @RequestParam("value") String value) {


        //I know I should be using a prepared statement to set these, but I kept getting a weird error
        //that I didnt get anywhere else, complaining that the syntax was wrong. Will fix if I have time.
        String insert = "INSERT INTO " + Const.DB_NAME + "." + table + " (" + table + ") values ('" + value+ "')";

        try {

            PreparedStatement insertStatement = rest.WidgetApplication.conn.prepareStatement(insert);
            insertStatement.execute();
        }catch(SQLException e){
            e.printStackTrace();
            return new ResponseEntity<>(gson.toJson(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Map<String, Object> result = new HashMap<>();

        result.put("result", "Success");

        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }

    @ApiOperation(value = "Gets all non-essencial attribute names")
    @RequestMapping(value = "/data/attributes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getAttributes() {
        List<String> attributes = new ArrayList<>();
        String get = "SELECT " + Const.ATTRIBUTE + " from " + Const.DB_NAME + "." + Const.ATTRIBUTE;

        try {
            PreparedStatement getStatement = rest.WidgetApplication.conn.prepareStatement(get);
            ResultSet rs = getStatement.executeQuery();
            while(rs.next()){
                attributes.add(rs.getString(Const.ATTRIBUTE));
            }

        }catch(SQLException e){
            e.printStackTrace();
            return new ResponseEntity<>(gson.toJson(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Map<String, Object> result = new HashMap<>();

        result.put("result", attributes);

        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }

    @ApiOperation(value = "Get's all possible values for the give attribute")
    @RequestMapping(value = "/data/attribute/values", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getAttributesLists(@RequestParam("attribute") String attribute) {
        List<String> values = new ArrayList<>();
        String get = "SELECT * from " + Const.DB_NAME + "." + attribute;

        try {
            PreparedStatement getStatement = rest.WidgetApplication.conn.prepareStatement(get);
            ResultSet rs = getStatement.executeQuery();
            while(rs.next()){
                values.add(rs.getString(attribute));
            }

        }catch(SQLException e){
            e.printStackTrace();
            return new ResponseEntity<>(gson.toJson(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Map<String, Object> result = new HashMap<>();

        result.put("result", values);

        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }


    @ApiOperation(value = "Takes in a widget object and adds it to inventory")
    @RequestMapping(value = "/data/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> setQuantity(@RequestParam("widget") String widgetJson) {
        Map<String, Object> widget = gson.fromJson(widgetJson, Map.class);
        List<String> attributeKeys = new ArrayList<>();

        String insert = "INSERT INTO " + Const.DB_NAME + "." + Const.INVENTORY + "  ("
                + Const.WIDGET + "," +  Const.CATEGORY + "," +  Const.PRICE +  "," +  Const.QUANTITY;
        for(String key : widget.keySet()){
            if(!Const.REQUIRED_FEILDS.contains(key)){
                insert = insert + "," + key;
                attributeKeys.add(key);
            }
        }

        insert = insert + ") VALUES (?,?,?,?";

        for(String key : attributeKeys){
            insert = insert + ",?" ;
        }
        insert = insert + ")";

        try {
            PreparedStatement widgetStatement = rest.WidgetApplication.conn.prepareStatement(insert);
            widgetStatement.setInt(1, getIdFromName(Const.WIDGET, widget.get(Const.WIDGET).toString()));
            widgetStatement.setInt(2, getIdFromName(Const.CATEGORY, widget.get(Const.CATEGORY).toString()));
            widgetStatement.setInt(3, (int) Float.parseFloat(widget.get(Const.PRICE).toString()));
            widgetStatement.setInt(4, (int) Float.parseFloat(widget.get(Const.QUANTITY).toString()));
            int index = 5;
            for(String key : attributeKeys){
                widgetStatement.setInt(index, getIdFromName(key, widget.get(key).toString()));
                index++;
            }

            widgetStatement.execute();
        }catch(SQLException e){
            e.printStackTrace();
            return new ResponseEntity<>(gson.toJson(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Map<String, Object> result = new HashMap<>();

        result.put("result", "Success");

        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }


    /**
     * This method takes a table and value, and gets the corresponding id for that value
     * @param table - name of table
     * @param name - value in table
     * @return
     * @throws SQLException
     */
    private int getIdFromName(String table, String name) throws SQLException{
        String query = "SELECT " + Const.ID + " FROM " + Const.DB_NAME + "." + table + " WHERE " + table + "=?";
        PreparedStatement queryStatement = rest.WidgetApplication.conn.prepareStatement(query);
        queryStatement.setString(1, name);
        ResultSet rs = queryStatement.executeQuery();
        rs.next();
        return rs.getInt("id");
    }

    /**
     * This endpoint takes in multiple parameters that specify what data to return.  It then constructs a
     * query and returns the valid data in json form.
     *
     * @param categoriesJson - List of category names that are valid
     * @param attributesJson - Map with attribute names as keys, and list of valid values
     * @param available - true if available only, false if you want out of stock items returned
     * @return
     */
    @ApiOperation(value = "Returns the widgets that meet the specified criteria", response = Item.class)
    @RequestMapping(value = "/data/query", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getSearchData(@RequestParam("categories") String categoriesJson,
                                                    @RequestParam("attributes") String attributesJson,
                                                    @RequestParam("available") Boolean available) {

        ArrayList<Object> json = null;

        // Parse list of categories to a list
        List<String> categories = gson.fromJson(categoriesJson, List.class);
        // Parse map of attirubtes and constraints to list
        Map<String, List<String>> attributes =  gson.fromJson(attributesJson, Map.class);

        try {
            // Add a where clause to the base query that will be built upon
            String query = createBaseQuery() + " where ";

            // If there are category constraints, add them
            if(categories.size() > 0){
                query +=  Const.DB_NAME + "." + Const.CATEGORY + "." + Const.CATEGORY + " in (";

                // Add parameters to query
                query = addConstraints(query, categories);
                query += " and ";
            }

            // if there are attribute constraints, add them
            if(attributes.entrySet().size() > 0) {
                // Add parameters to attribute query
                for (Map.Entry<String, List<String>> attributeQuery : attributes.entrySet()) {
                    query += Const.DB_NAME + "." + attributeQuery.getKey() + "." + attributeQuery.getKey() + " in (";
                    query = addConstraints(query, attributeQuery.getValue());
                    query += " and ";
                }

            }

            // if the query is for only available item, make quantity over 0.
            if(available){
                query += Const.DB_NAME + "." + Const.INVENTORY + "." + Const.QUANTITY + " > 0";
            } else{
                query += Const.DB_NAME + "." + Const.INVENTORY + "." + Const.QUANTITY + "> -1";
            }

            // Create prepared statement with parameterized query
            PreparedStatement categoryData = rest.WidgetApplication.conn.prepareStatement(query);

            // Fill in parameters with category values, as to protect against SQL injection.
            int statementIndex = 1;
            for(int i = 0; i < categories.size(); i++){
                categoryData.setString(statementIndex , categories.get(i));
                statementIndex++;
            }

            // Fill in parameters with category values, as to protect against SQL injection.
            for(Map.Entry<String, List<String>> attributeQuery : attributes.entrySet()){
                for(int i = 0; i < attributeQuery.getValue().size(); i++){
                    categoryData.setString(statementIndex , attributeQuery.getValue().get(i));
                    statementIndex++;
                }
            }
            ResultSet resultSet = categoryData.executeQuery();
            json = convertResultSetToJson(resultSet);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, Object> result = new HashMap<>();

        result.put("result", json);


        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }


    /**
     * This method add a list of constraints to a 'where in' clause
     * @param query  - Query string to append
     * @param constraints - List of constraints (valid values)
     * @return
     */
    private static String addConstraints(String query, List<String> constraints){
        for (int i = 0; i < constraints.size(); i++) {
            if (i < constraints.size() - 1) {
                query += "?,";
            } else {
                query += "?)";
            }
        }

        return query;
    }

    /**
     * This method will generate a sql query that returns all of the feilds and rows from the Const.INVENTORY table.
     * It inner joins these feilds to get the attribute values.  It learns which attributes to grab from the
     * attributes table, so this method is dynmaic to changes in attribtues.
     *
     * @return
     * @throws SQLException
     */
    public static String createBaseQuery() throws SQLException{

        // Create the base of the query, these feilds are required for widget items and will not go away
        String query = "SELECT "
                + Const.DB_NAME + "." + Const.INVENTORY + "." + Const.ID + ", "
                + Const.DB_NAME + "." + Const.INVENTORY + "." + Const.PRICE + ", "
                + Const.DB_NAME + "." + Const.INVENTORY + "." + Const.QUANTITY + ", "
                + Const.DB_NAME + "." + Const.WIDGET + "." + Const.WIDGET + ", "
                + Const.DB_NAME + "." + Const.CATEGORY + "." + Const.CATEGORY;

        //Get the list of attributes that exist on widgets
        PreparedStatement attributes = rest.WidgetApplication.conn.prepareStatement("select " + Const.DB_NAME + "." + Const.ATTRIBUTE + "." + Const.ATTRIBUTE + " from " + Const.DB_NAME + "." + Const.ATTRIBUTE);

        // Get set of attributes and add them to list of colums to return
        ResultSet attributeSet = attributes.executeQuery();
        ArrayList<String> attributesList = new ArrayList<String>();
        attributesList.add(Const.WIDGET);
        attributesList.add(Const.CATEGORY);

        while (attributeSet.next()) {
            String attr = attributeSet.getString(1);
            attributesList.add(attr);
            query = query + ", " + Const.DB_NAME + "." + attr + "." + attr;
        }

        query = query + " FROM " + Const.DB_NAME + "." + Const.INVENTORY + " ";

        // Add inner joins for all attributes, to get values from their respective tables.
        for(String attr : attributesList){
            query = query + " INNER JOIN " + Const.DB_NAME + "." + attr +
                    " ON " + Const.DB_NAME + "." + attr + ".id = " + Const.DB_NAME + "." + Const.INVENTORY + "." + attr;
        }

        return query;
    }

    public static ArrayList<Object> convertResultSetToJson(ResultSet resultSet) throws SQLException {
        ArrayList<Object> list = new ArrayList<>();
        while (resultSet.next()) {
            int total_rows = resultSet.getMetaData().getColumnCount();
            JsonObject obj = new JsonObject();
            for (int i = 1; i < total_rows  +1; i++) {
                obj.addProperty(resultSet.getMetaData().getColumnLabel(i)
                        .toLowerCase(), resultSet.getObject(i).toString());
            }
            list.add(obj);
        }
        return list;
    }

}
