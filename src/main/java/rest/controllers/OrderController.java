package rest;

import com.google.gson.Gson;
import io.swagger.annotations.ApiOperation;
import main.java.model.Const;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/api")
public class OrderController {
    private static Gson gson = new Gson();


    @ApiOperation(value = "Creates a new order id and returns it to the front end.")
    @RequestMapping(value = "/order/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> createOrder() {

        //Create a unique identifier for this order
        UUID uuid = UUID.randomUUID();
        Map<String, Object> result = new HashMap<>();
        result.put("result", uuid.toString());

        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }

    @ApiOperation(value = "Deletes the order with the specified id.")
    @RequestMapping(value = "/order/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> deleteOrder(@RequestParam("orderId") String orderId) {
        String orderItems =  "SELECT " + Const.DB_NAME + "." + Const.ORDERS + "." + Const.WIDGET_ID +
                " FROM " + Const.DB_NAME + "." + Const.ORDERS + " WHERE " + Const.DB_NAME + "." + Const.ORDERS + "." +  Const.ID + " = ?";

        try {
            //Get items in the order, remove them, and increment their inventory
            PreparedStatement itemsStatement = rest.WidgetApplication.conn.prepareStatement(orderItems);
            itemsStatement.setString(1, orderId);
            ResultSet resultSet = itemsStatement.executeQuery();
            while(resultSet.next()){
                int widgetId = resultSet.getInt(Const.WIDGET_ID);
                removeItemFromOrder(orderId, widgetId);
                incrementInverntory(widgetId);
            }

        }catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(gson.toJson(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("result", "Deleted Successfully");

        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }


    @ApiOperation(value = "Gets all items from an order and returns them in json form.")
    @RequestMapping(value = "/order/items", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getOrderItem(@RequestParam("orderId") String orderId) {
        ArrayList<Integer> itemIds = new ArrayList<>();
        try {

            //Select statements for correct item id's based on orderId
            String orderItems =  "SELECT " + Const.DB_NAME + "." + Const.ORDERS + "." + Const.WIDGET_ID +
                    " FROM " + Const.DB_NAME + "." + Const.ORDERS + " WHERE " + Const.DB_NAME + "." + Const.ORDERS + "." +  Const.ID + " = ?";


            PreparedStatement allItemsStatement = rest.WidgetApplication.conn.prepareStatement(orderItems);
            allItemsStatement.setString(1, orderId);

            ResultSet resultSet = allItemsStatement.executeQuery();
            while(resultSet.next()){
                itemIds.add(resultSet.getInt(Const.WIDGET_ID));
            }
        }catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(gson.toJson(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("result", itemIds);

        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }

    @ApiOperation(value = "Gets details from an item")
    @RequestMapping(value = "/order/item/details", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getOrderItemDetails(@RequestParam("itemId") int itemId) {
        ArrayList<Object> json = null;
        try {
            String query = rest.DataController.createBaseQuery();
            query += " WHERE " + Const.DB_NAME+ "." + Const.INVENTORY + "." +Const.ID + " = ?";

            PreparedStatement itemStatement = rest.WidgetApplication.conn.prepareStatement(query);
            itemStatement.setInt(1, itemId);

            ResultSet resultSet = itemStatement.executeQuery();
            json = rest.DataController.convertResultSetToJson(resultSet);
        }catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(gson.toJson(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("result", json);

        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }



    @ApiOperation(value = "Adds the specified item to the order with the specified orderId.")
    @RequestMapping(value = "/order/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> addItemToOrder(@RequestParam("widgetId") int widgetId, @RequestParam("orderId") String orderId) {
        try {
            // Create order and add widgets to database, then increment the inventory to match.
            addOrderToDatabase(orderId, widgetId);
            decrementInverntory(widgetId);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(gson.toJson(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        Map<String, Object> result = new HashMap<>();

        result.put("result", orderId);

        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }

    @ApiOperation(value = "Deletes a specified item from the specified order")
    @RequestMapping(value = "/order/remove", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> deleteItemFromOrder(@RequestParam("widgetId") int widgetId, @RequestParam("orderId") String orderId) {
        try {
            // Create order and add widgets to database, then increment the inventory to match.
            removeItemFromOrder(orderId, widgetId);
            incrementInverntory(widgetId);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(gson.toJson(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        Map<String, Object> result = new HashMap<>();

        result.put("result", orderId);

        return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
    }


    /**
     * This function takes in a widget id and order id, and will remove the first occurance of that pair from the order
     * @param widgetId - id of inventory item to increment
     * @param orderId - id for the order
     * @return
     * @throws SQLException
     */
    private static boolean removeItemFromOrder(String orderId, int widgetId) throws SQLException {
        // DELETE FROM widgetdb.orders where id = 'f5bfa3fd-f793-4adb-9a57-1ba499bfbf72' and widgetid = 3 LIMIT 1;
        String deleteOrderItem = "DELETE FROM " +  Const.DB_NAME + "." + Const.ORDERS +  " WHERE " +
                Const.ID + " = ? AND " + Const.WIDGET_ID + " = ? LIMIT 1";
        // Create prepared statement with parameterized query
        PreparedStatement deleteOrderItemStatement = rest.WidgetApplication.conn.prepareStatement(deleteOrderItem);

        // Fill in parameters with category values, as to protect against SQL injection.
        deleteOrderItemStatement.setString(1, orderId);
        deleteOrderItemStatement.setInt(2, widgetId);
        return deleteOrderItemStatement.execute();
    }

    /**
     * This function takes in a widget id and will increment the quantity in the inventory table.
     * @param widgetId - id of inventory item to increment
     * @return
     * @throws SQLException
     */
    private static boolean incrementInverntory(int widgetId) throws SQLException {
        String increment = "UPDATE " +  Const.DB_NAME + "." + Const.INVENTORY +  " SET " +
                Const.DB_NAME + "." + Const.INVENTORY + "." + Const.QUANTITY + " = " + Const.DB_NAME + "." + Const.INVENTORY + "." + Const.QUANTITY +"+1" +
                " WHERE " + Const.ID + "=?";
        // Create prepared statement with parameterized query
        PreparedStatement incrementStatement = rest.WidgetApplication.conn.prepareStatement(increment);

        // Fill in parameters with category values, as to protect against SQL injection.
        incrementStatement.setInt(1, widgetId);
        return incrementStatement.execute();
    }

    /**
     * This function takes in a widget id and will decrement the quantity in the inventory table.
     * @param widgetId - id of inventory item to decrement
     * @return
     * @throws SQLException
     */
    private static boolean decrementInverntory(int widgetId) throws SQLException {
        String decrement = "UPDATE " +  Const.DB_NAME + "." + Const.INVENTORY +  " SET " +
                Const.DB_NAME + "." + Const.INVENTORY + "." + Const.QUANTITY + " = " + Const.DB_NAME + "." + Const.INVENTORY + "." + Const.QUANTITY +"-1" +
                " WHERE " + Const.ID + "=?" + " AND " + Const.DB_NAME + "." + Const.INVENTORY + "." + Const.QUANTITY + " > 0";
        // Create prepared statement with parameterized query
        PreparedStatement decrementStatement = rest.WidgetApplication.conn.prepareStatement(decrement);

        // Fill in parameters with category values, as to protect against SQL injection.
        decrementStatement.setInt(1, widgetId);
        return decrementStatement.execute();
    }

    /**
     * This function will take in an orderid and widget id and add a row to the orders database
     * @param orderId - order id
     * @param widgetId -widget id
     * @return
     * @throws SQLException
     */
    private static boolean addOrderToDatabase(String orderId, int widgetId) throws SQLException{
        String insert = "INSERT into " + Const.DB_NAME + "." + Const.ORDERS +
                " (" + Const.DB_NAME + "." + Const.ORDERS + "." + Const.ID + ","
                + Const.DB_NAME + "." + Const.ORDERS + "."+ Const.WIDGET_ID + ") values (?,?)";

        // Create prepared statement with parameterized query
        PreparedStatement insertStatement = rest.WidgetApplication.conn.prepareStatement(insert);

        // Fill in parameters with category values, as to protect against SQL injection.
        insertStatement.setString(1, orderId);
        insertStatement.setInt(2, widgetId);

        return insertStatement.execute();
    }
}
