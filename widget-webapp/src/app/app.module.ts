import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
   MatToolbarModule,
   MatSidenavModule,
   MatButtonModule,
   MatIconModule,
   MatCardModule,
   MatCheckboxModule,
   MatChipsModule,
   MatInputModule,
   MatSlideToggleModule,
   MatExpansionModule,
   MatSelectModule
 } from '@angular/material';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { WidgetListComponent } from './widget-list/widget-list.component';
import { WidgetComponent } from './widget/widget.component';
import { DataService } from './services/data.service';
import { CartComponent } from './cart/cart.component';
import { CartItemComponent } from './cart-item/cart-item.component';
import { BrowseComponent } from './browse/browse.component';
import { EditComponent } from './edit/edit.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home/none',  pathMatch: 'full'},
  { path: 'home/:orderId', component: BrowseComponent },
  { path: 'cart/:orderId', component: CartComponent },
  { path: 'edit/:orderId', component: EditComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    WidgetListComponent,
    WidgetComponent,
    CartComponent,
    CartItemComponent,
    BrowseComponent,
    EditComponent
    
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    MatToolbarModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    HttpClientModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    RouterModule,
    MatInputModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatSelectModule
  ],
  providers: [DataService, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }



