import { Component, OnInit, Input } from '@angular/core';
import { isNgTemplate } from '@angular/compiler';
import { DataItem } from '../classes/data-item';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css']
})
export class CartItemComponent implements OnInit {
  
 objectKeys = Object.keys;
  @Input() item: DataItem;
  
  constructor(public dataService:DataService) { }

  ngOnInit() {
  }

  removeFromOrder(){
    this.dataService.removeFromOrder(this.dataService.orderId, this.item);
  }

}
