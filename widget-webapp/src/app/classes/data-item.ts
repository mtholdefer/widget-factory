export class DataItem{
    public id: number;
    public price: number;
    public widget: string;
    public category: string;
    public quantity: number;
    [x: string]: any;
}