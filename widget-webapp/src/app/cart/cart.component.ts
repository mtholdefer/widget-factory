import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../services/data.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  objectKeys = Object.keys;
  sub: any;
  id: string;
  formId: string;
  constructor(private route: ActivatedRoute, public dataService: DataService, private router:Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['orderId'];
      if (this.id != 'none') {
        this.dataService.getOrderItems(this.id).subscribe(response => {
          let ids: number[] = response.result;
          this.dataService.setOrderItems(ids);
          this.dataService.orderId = this.id;
        })
      }
    });
  }

  searchOrderId() {
    this.dataService.getOrderItems(this.formId).subscribe(response => {
      let ids: number[] = response.result;
      this.dataService.setOrderItems(ids);
      this.dataService.orderId = this.formId;
      this.router.navigate(['/cart', this.dataService.orderId]);
    })
  }

  deleteOrder(){
    this.dataService.deleteOrder(this.id);
    this.dataService.orderId = "none"
  }

}
