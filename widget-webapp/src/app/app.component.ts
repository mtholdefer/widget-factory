import { Component, OnInit } from '@angular/core';
import { Routes, Router } from '@angular/router';
import * as _ from 'lodash'
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(public dataService: DataService, private router: Router){}
  goToCart(){
    console.log(this.dataService.orderId);
    if(!this.dataService.orderId) { this.dataService.orderId = 'none'}
    this.router.navigate(['/cart/', this.dataService.orderId]);
  }
  
  ngOnInit(){
    this.dataService.getAllData().subscribe(response => {
      this.dataService.all = response.result;
      this.dataService.updateAttributeLists();
		});
  }
  navHome(){
    console.log(this.dataService.orderId);
    this.router.navigate(['/home/', this.dataService.orderId])
  }
}
