import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DataService } from '../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.css']
})
export class BrowseComponent implements OnInit {
  sub: any;
  id: string;
  
  constructor(private route: ActivatedRoute, public dataService: DataService, private router: Router){
  }

  updateFilter(){
    this.dataService.updateData();
  }

  editData(){
    this.router.navigate(['edit', this.dataService.orderId])
  }
ngOnInit(){
   this.sub = this.route.params.subscribe(params => {
      this.id = params['orderId'];
      if (this.id != 'none') {
        this.dataService.getOrderItems(this.id).subscribe(response => {
          let ids: number[] = response.result;
          this.dataService.setOrderItems(ids);
          this.dataService.orderId = this.id;
        })
      }
   });
  
}

}
