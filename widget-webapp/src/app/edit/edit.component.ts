import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { DataItem } from '../classes/data-item';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  newWidget:DataItem;
  newAttributeObject: any;
  newWidgetType: string;
  category: string;

  constructor(public dataService: DataService) {
    this.newAttributeObject = {};
  }

  ngOnInit() {
    this.dataService.getAllData().subscribe(response => {
      this.dataService.all = response.result;
    })
    this.newWidget = new DataItem;
  }

  updateQuantity(id:number, quant:number){
    this.dataService.setQuantity(id, quant);
  }

  addWidget(){
    console.log(this.newWidget);
    this.dataService.addWidget(this.newWidget);
    this.newWidget = new DataItem();
  }
}
