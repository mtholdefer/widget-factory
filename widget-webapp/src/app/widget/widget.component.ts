import { Component, OnInit, Input } from '@angular/core';
import { DataItem } from '../classes/data-item';
import * as _ from 'lodash';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css']
})
export class WidgetComponent implements OnInit {
  @Input() widget: DataItem;
  objectKeys = Object.keys;
  constructor(private dataService: DataService) { }

  ngOnInit() {
    
  }

  addToOrder(){
    this.dataService.addToOrder(this.widget);
    this.dataService.orderItems.push(this.widget);
  }

}
