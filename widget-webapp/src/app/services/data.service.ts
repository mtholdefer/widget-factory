import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { DataItem } from '../classes/data-item';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { share } from 'rxjs/operators';
import * as _ from 'lodash';
import { Router } from '@angular/router';

@Injectable()
export class DataService {

  //keys for immutable properties that all items must have
  BASE_KEYS = ['id', 'price', 'widget', 'category', 'quantity'];
  BASE_TABLES = ['widget', 'category'];
  all: DataItem[];
  orderItems: DataItem[];
  orderId: string;
  widgets: string[];
  categories: { name: string, selected: boolean }[];
  attributes: { name: string, options: {name: string, selected: boolean}[]};
  attributesList: string[];
  available: boolean = false;


  constructor(private httpClient: HttpClient, private router: Router) { 
    this.orderId = 'none';
    this.orderItems = [];
    this.widgets = [];
    this.attributes = {} as { name: string, options: {name: string, selected: boolean}[]};
  }

  updateData() {
    this.queryData().subscribe(response => {
      this.all = response.result;
    });
  }

  getQueryParameters(){
    let params = {};
    params['categories'] = JSON.stringify(this.getSelectedList(this.categories));
    params['attributes'] = JSON.stringify(this.getSelectedAttributes(this.attributes));
    params['available'] = this.available;
    return params;
  }

  addDataValue(attribute:string, value: string){
    let params = new HttpParams()
        .append("table", attribute)
        .append("value", value);

      let observable = this.post('data/value/add', params);
      observable.subscribe(response => {
        this.updateAttributeLists();
      })
  }

  

  getItemDetails(itemId){
    let params = { itemId: itemId};
    let observable = this.get('order/item/details', params) as Observable<any>;
    
    // Return Graph object.
    return observable;
  }

  getOrderItems(orderId){
    let params = { orderId: orderId};
    let observable = this.get('order/items', params) as Observable<any>;
    
    // Return Graph object.
    return observable;
  }

  getSelectedList(object){
   return object
              .filter(opt => opt.selected)
              .map(opt => opt.name)
  
  }

  getSelectedAttributes(object){
    let selected = {};
    _.forEach(this.attributesList, key =>{
      selected[key] = this.getSelectedList(this.attributes[key]);
    })
    return selected;
   }

   addWidget(widget:DataItem){
     let params = new HttpParams()
        .append("widget", JSON.stringify(widget));

      let observable = this.post('data/add', params);
      observable.subscribe(response => {
        this.updateData();
      })
   }

   queryData(){
    let params = this.getQueryParameters();
    let observable = this.get('data/query', params) as Observable<any>;

    // Print output (for debugging)
    observable.subscribe(response => {
      console.log('Query response', response.result);
    });

    // Return Graph object.
    return observable;
  }

  getAllData() {
    let observable = this.get('data/all', {}) as Observable<any>;

    // Print output (for debugging)
    observable.subscribe(response => {
      console.log('Query response', response.result);
    });

    // Return Graph object.
    return observable;
  }

  addToOrder(widget: DataItem) {
    if (this.orderId != 'none' && this.orderId != undefined) {
      let params = new HttpParams()
        .append("orderId", this.orderId)
        .append("widgetId", widget.id.toString())
  
      let observable = this.post('order/add', params);
      observable.subscribe(response => {
        this.updateData();
      })
    } else {
      let observable = this.post('order/create', null);
      observable.subscribe(response => {
        this.orderId = response.result
        console.log("New Order: ", this.orderId)
        this.addToOrder(widget);
      })
    }
  }

  setOrderItems(ids: number[]){
    this.orderItems = [];
    _.forEach(ids, id => {
      this.getItemDetails(id).subscribe(resp => {
        this.orderItems.push(resp.result[0]);
      })
    })
    
  }

  setQuantity(itemId: number, quantity: number){
    
    let params = new HttpParams()
        .append("itemId", itemId.toString())
        .append("quantity", quantity.toString())
  
      let observable = this.post('data/set/quantity', params);
      observable.subscribe(response => {
        this.updateData();
      })
  }
  
  deleteOrder(orderId){
    
    let params = new HttpParams()
        .append("orderId", orderId)

      let observable = this.post('/order/delete', params);
      observable.subscribe(response => {
        this.updateData();
        this.orderItems = [];
        this.router.navigate(['cart', 'none'])
      })
  }

  removeFromOrder(orderId: string, widget:DataItem){
    let params = new HttpParams()
        .append("orderId", orderId)
        .append("widgetId", widget.id.toString())
  
      let observable = this.post('order/remove', params);
      observable.subscribe(response => {
        this.orderItems = _.without(this.orderItems, widget);
        this.updateData();
      })
  }

  updateAttributeLists(){
    let observable = this.get('data/attributes', {}) as Observable<any>;
    // Print output (for debugging)
    observable.subscribe(response => {
      this.categories = [];
      this.widgets = [];
      this.attributesList = response.result

      this.getAttributeValues('category').subscribe(resp => {
        _.forEach(resp.result, val =>{
          this.categories.push({name: val, selected: true});
        })
      })

      this.getAttributeValues('widget').subscribe(resp => {
        this.widgets = resp.result
      })

      _.forEach(this.attributesList, attr =>{
        this.attributes[attr] = [];
        this.getAttributeValues(attr).subscribe(resp => {
          _.forEach(resp.result, val =>{
            this.attributes[attr].push({name: val, selected: true});
          })
        })
      })
    });
  }
  
  getAttributeValues(attribute:string){
    let observable = this.get('data/attribute/values', {attribute}) as Observable<any>;
    return observable;
  }


  private get(path: string, params: { [key: string]: any }): Observable<any> {
    return (
      this.httpClient
        .get(environment.apiPath + path, {
          params: params,
        })
        // Ensure all subscribers share the same http call, rather than generate new ones:
        .pipe(share())
    );
  }

  private post(path: string, params: HttpParams): Observable<any> {
    return (
      this.httpClient
        .post(environment.apiPath + path, params)
        // Ensure all subscribers share the same http call, rather than generate new ones:
        .pipe(share())
    );
  }


}
