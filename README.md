# widget-factory

Hosted location: http://ec2-3-16-214-17.us-east-2.compute.amazonaws.com:9090

API Documentation: http://ec2-3-16-214-17.us-east-2.compute.amazonaws.com:9090/swagger-ui.html

## Build Process

In the widget-webapp folder, run:

`npm install`

`npm run build`

From the base folder, run:

`mvn clean package`

The command to run the application:

`java -jar target/widget-factory.jar`


This will access a local mysql database instance that has a database named `widgetdb`.  The MySQL dump for this is located in the `data` folder.


Bug List:

	- Add validator to quantity input to disallow values below 0.
	- Add validator to price input to disallow values below 0.