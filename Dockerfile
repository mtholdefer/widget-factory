FROM maven:3.5.4-alpine AS build-env

RUN apk add --update nodejs nodejs-npm openjdk8
RUN apk add build-base

COPY . ./project

# Enter front end directory
WORKDIR ./project/widget-webapp/

# Install and compile Front End
RUN npm install
RUN npm run build

# Enter base project directory
WORKDIR /project/

# Package to jar via maven
RUN mvn clean package -Dmaven.test.skip=true -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true

RUN cp /project/src/main/resources/application.properties /project/target/


FROM alpine
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
RUN apk add openjdk8
WORKDIR /app
COPY --from=build-env /project/target/* /app/

EXPOSE 8080

CMD java -jar widget-factory.jar --dev=false
